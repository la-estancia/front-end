<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="ReporteProductos.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.ReporteProductos" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" charset="UTF-8"></script>

<asp:scriptmanager runat="server"></asp:scriptmanager>
<asp:UpdatePanel ID="updPrincipal" UpdateMode="Conditional" runat="server">
<ContentTemplate>

    <asp:MultiView ID="mvlPrincipal" runat="server" ActiveViewIndex="0">
        <asp:View ID="vista1" runat="server">     

              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->                    
                
                <div class="col-md-10 col-md-offset-1">
                
                        <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Reporte de productos y combos</h3>
                            </div>
                        </center>
                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">                                        
                          Reporte de Productos
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos1" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoProducto">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoProducto" runat="server" CommandName="CodigoProducto" Text='<%# Eval("CodigoProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Descripcion" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDescripcionProducto" runat="server" CommandName="DescripcionProducto" Text='<%# Eval("DescripcionProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Precio venta" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrecioVenta" runat="server" CommandName="PrecioVenta" Text='<%# "Q " + Eval("PrecioVenta") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                        
                                                    </Columns>
                                                </asp:GridView>                                                                           
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>

                        <div id="modalOrdenNueva" class="modal fade bd-example-modal" data-keyboard="false" role="dialog" data-backdrop="static">
                                    <div class="modal-dialog" role="document">
                                        <!-- Modal content-->
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">                                       
                                        <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">                                                                                                                                                     
                                                <asp:LinkButton class="close" ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Cerrar</asp:LinkButton>                                                
                                                <h4 class="modal-title">Detalle de combo </h4>
                                            </div>
                                            <div class="modal-body">                                                    
                                                <asp:GridView ID="GrdDatos3" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoProducto">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedidoDetalle" runat="server" CommandName="CodigoPedidoDetalle" Text='<%# Eval("CodigoProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto " ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombre" runat="server" CommandName="lblNombre" Text='<%# Eval("DescripcionProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" CommandName="Cantidad" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        
                                                    </Columns>
                                                    </asp:GridView>                                                                           
                                            </div>                                                                                                  
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>



                        <div class="box-body">                                        
                          Reporte de Combos
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoCombo" OnRowCommand="GrdDatos2_RowCommand">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Combo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoCombo" runat="server" CommandName="CodigoCombo" Text='<%# Eval("CodigoCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Descripcion" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDescripcionCombo" runat="server" CommandName="DescripcionCombo" Text='<%# Eval("DescripcionCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Precio venta" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrecioCombo" runat="server" CommandName="PrecioCombo" Text='<%# "Q " + Eval("PrecioCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                        
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1   %" >
                                                            <ItemTemplate>
                                                                <center>                                                        
                                                                <asp:Button ID="btnDetallePedido" runat="server" class="btn btn-success" Text="Detalle Combo"  CommandName="btnDetallePedido" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>                                                        
                                                            </center>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                
                                                    </Columns>
                                                </asp:GridView>                                                                           
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>
                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </asp:View>

    </asp:MultiView>
</ContentTemplate> 
</asp:UpdatePanel>   
</asp:Content>
