using AppWebRestaurante.BackEnd;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebRestaurante.FrontEnd
{
    public partial class WpgMesas : System.Web.UI.Page
    {
        ClsMesas objeto = ClsMesas.getInstancia();
        ClsPedidos objeto2 = ClsPedidos.getInstancia();
        public Boolean blnError;
        public String error;
        public String diseño = "btn btn-success";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["CodigoMesaSel"] = 0;
                Session["MeseroSel"] = "";
                cargarInformacion();
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            prepararComboBox();
            mvlPrincipal.ActiveViewIndex = 1;
            Session["ACCION"] = "AGREGAR";            
            habilitarBotones(true);
        }

        protected void GrdDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GrdDatos.Rows[index];
            Label lblCodigoMesa = (Label)row.FindControl("lblCodigoMesa");
            Label lblDescripcionMesa = (Label)row.FindControl("lblDescripcionMesa");                        
            Session["CodigoMesaSel"] = lblCodigoMesa.Text;

            this.txtDescripcion.Text = lblDescripcionMesa.Text;            

            if (e.CommandName.Equals("btnOrdenes"))
            {
                txtTotalCuenta.Text = "";
                DataTable tablaCaja = objeto2.consultarCaja();
                DataTable pedido = objeto2.obtenerPedidoMesa(int.Parse(Session["CodigoMesaSel"].ToString()));
                foreach (DataRow registro in pedido.Rows)
                {
                    Session["CodigoPedidoSel"] = registro["CodigoPedido"].ToString();
                    txtTotalCuenta.Text = registro["TotalPedido"].ToString();
                    Session["TotalPedidoSel"] =  registro["TotalPedido"].ToString();
                }


                foreach (DataRow roww in tablaCaja.Rows)
                {
                    if (roww["EstadoCaja"].Equals(false))
                    {
                        error = "Caja no esta abierta";
                        String script1 = " $(function () {  $(\"#modalError\").modal(\"show\"); });";
                        ScriptManager.RegisterStartupScript(updDetalle, updDetalle.GetType(), "invokeModal", script1, true);
                        updPrincipal.Update();
                        return;
                    }
                }
                DataTable datosPedido = objeto2.consultarPedidoMesa(int.Parse(lblCodigoMesa.Text));
                foreach (DataRow registro in datosPedido.Rows)
                {
                    Session["CodigoPedidoSel"] = registro["CodigoPedido"].ToString();                    
                }

                prepararComboBox();

                if (datosPedido.Rows.Count > 0)
                {
                    String script1 = " $(function () {  $(\"#modalOrden\").modal(\"show\"); });";
                    ScriptManager.RegisterStartupScript(updDetalle, updDetalle.GetType(), "invokeModal", script1, true);
                    updPrincipal.Update();
                    consultarOrden(int.Parse(lblCodigoMesa.Text));
                } else
                {
                    String script1 = " $(function () {  $(\"#modalOrdenNueva\").modal(\"show\"); });";
                    ScriptManager.RegisterStartupScript(updDetalle, updDetalle.GetType(), "invokeModal", script1, true);
                    updPrincipal.Update();                   
                }
            }            
            
        }

        public void consultarOrden(int codigoPedidoMesa)
        {                        
            updPrincipal.Update();
            GrdDatos2.DataSource = objeto2.consultarPedidoMesa(codigoPedidoMesa);
            GrdDatos2.DataBind();

            if (GrdDatos2.Rows.Count > 0)
            {
                GrdDatos2.UseAccessibleHeader = true;
                GrdDatos2.HeaderRow.TableSection = TableRowSection.TableHeader;
                GrdDatos2.FooterRow.TableSection = TableRowSection.TableFooter;
                string script1 = "$('#ContentPlaceHolder1_GrdDatos2').DataTable({ \"scrollY\":        \"270px\", \"scrollCollapse\": false, \"paging\":         false   });";
                ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "ssss", script1, true);
            }
            
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtDescripcion.Text.Equals(""))
                {
                    mostrarError("Campo descripción en blanco");
                    return;
                }
                
                //objeto.mantenimientoRegistro(Session["ACCION"].ToString(), int.Parse(dpdCodigoNegocio.SelectedValue.ToString()), int.Parse(dpdCodigoEmpresa.SelectedValue.ToString()), int.Parse(Session["CodigoUsuario"].ToString()), txtNombre.Text, txtApellido.Text, txtCorreo.Text, txtTelefono.Text, txtLogin.Text, txtPassword.Text, int.Parse(dpdRol.SelectedValue.ToString()));
                

                mvlPrincipal.ActiveViewIndex = 0;
                cargarInformacion();
                this.txtDescripcion.Text = "";                
            }
            catch (Exception ex)
            {
                mostrarError("ERROR EN BD : " + ex.Message);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            mvlPrincipal.ActiveViewIndex = 0;
        }


        public void cargarInformacion()
        {
            updPrincipal.Update();
            mvlPrincipal.ActiveViewIndex = 0;
            GrdDatos.DataSource = objeto.consultarMesas();
            GrdDatos.DataBind();            
        }

        public void habilitarBotones(Boolean habilitar)
        {
            this.txtDescripcion.Enabled = habilitar;            
        }

        private void mostrarError(String errorApp)
        {
            // Muestra la ventana modal del error 
            error = errorApp;
            blnError = true;
            String script1 = " $(function () {  $(\"#modalError\").modal(\"show\"); });";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "invokeModal", script1, true);
            updPrincipal.Update();
            return;
        }

        public void prepararComboBox()
        {
            dpdMesero.DataSource = objeto.consultarMesero();
            dpdMesero.DataTextField = "NombreUsuario";
            dpdMesero.DataValueField = "CodigoUsuario";
            dpdMesero.DataBind();

            dpdProducto.DataSource = objeto.consultarProductos();
            dpdProducto.DataTextField = "DescripcionProducto";
            dpdProducto.DataValueField = "CodigoProducto";
            dpdProducto.DataBind();
            dpdProducto.SelectedIndex = 0;

            dpdCombo.DataSource = objeto.consultarCombos();
            dpdCombo.DataTextField = "DescripcionCombo";
            dpdCombo.DataValueField = "CodigoCombo";
            dpdCombo.DataBind();
            dpdCombo.SelectedIndex = 0;
        }

        protected void lnkCerrarModal_Click(object sender, EventArgs e)
        {
            cargarInformacion();
            cerrarModalPedidos();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            DataTable pedido = objeto2.obtenerPedidoMesa(int.Parse(Session["CodigoMesaSel"].ToString()));
            foreach (DataRow registro in pedido.Rows)
            {
                Session["CodigoPedidoSel"] = registro["CodigoPedido"].ToString();
                txtTotalCuenta.Text = registro["TotalPedido"].ToString();
                Session["TotalPedidoSel"] = registro["TotalPedido"].ToString();
            }
            // Grabar producto / Combo 
            int codigoPedido = int.Parse(Session["CodigoPedidoSel"].ToString());
            int codigoProducto = int.Parse(dpdProducto.SelectedValue);
            int codigoCombo = int.Parse(dpdCombo.SelectedValue);
            int cantidad = 0;
            if (codigoProducto > 0)
            {
                cantidad = int.Parse(txtCantidadproducto.Text);
            }else
            {
                cantidad = int.Parse(txtCantidadCombo.Text);
            }                        
            objeto2.crearDetallePedido(codigoPedido, codigoProducto, codigoCombo, cantidad);
            pedido = objeto2.obtenerPedidoMesa(int.Parse(Session["CodigoMesaSel"].ToString()));
            txtTotalCuenta.Text = "";
            foreach (DataRow registro in pedido.Rows)
            {
                Session["CodigoPedidoSel"] = registro["CodigoPedido"].ToString();
                txtTotalCuenta.Text = registro["TotalPedido"].ToString();
                Session["TotalPedidoSel"] = registro["TotalPedido"].ToString();
            }
            String script1 = " $(function () {  $(\"#modalOrden\").modal(\"show\"); });";
            ScriptManager.RegisterStartupScript(updDetalle, updDetalle.GetType(), "invokeModal", script1, true);
            updPrincipal.Update();
            consultarOrden(int.Parse(Session["CodigoMesaSel"].ToString()));
            prepararComboBox();
            txtCantidadCombo.Text = "";
            txtCantidadproducto.Text = "";
            
        }

        protected void btnCancelarDetalle_Click(object sender, EventArgs e)
        {

        }

        protected void BtnCrearOrden_Click(object sender, EventArgs e)
        {
            int codigoMesero = int.Parse(dpdMesero.SelectedValue.ToString());
            int codigoMesa = int.Parse(Session["CodigoMesaSel"].ToString());
            Session["MeseroSel"] = dpdMesero.SelectedItem.ToString();
            objeto2.crearPedido(codigoMesero, codigoMesa);

            cerrarModalPedidos();
            mostrarModalPedidos();

        }

        protected void GrdDatos2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GrdDatos2.Rows[index];
            Label lblCodigoPedido = (Label)row.FindControl("lblCodigoPedido");
            Label lblCodigoPedidoDetalle = (Label)row.FindControl("lblCodigoPedidoDetalle");
            

            if (e.CommandName.Equals("btnEliminar"))
            {
                objeto2.quitarProducto(int.Parse(lblCodigoPedido.Text), int.Parse(lblCodigoPedidoDetalle.Text));
                cerrarModalPedidos();
                mostrarModalPedidos();
                DataTable pedido = objeto2.obtenerPedidoMesa(int.Parse(Session["CodigoMesaSel"].ToString()));
                foreach (DataRow registro in pedido.Rows)
                {
                    Session["CodigoPedidoSel"] = registro["CodigoPedido"].ToString();
                    txtTotalCuenta.Text = registro["TotalPedido"].ToString();
                    Session["TotalPedidoSel"] = registro["TotalPedido"].ToString();
                }

            }
        }

        public void cerrarModalPedidos()
        {
            string script1 = "$('#modalOrden').modal('hide');";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "ssss", script1, true);
            string script2 = " $('.modal-backdrop.fade').css({ 'background-color': 'rgba(0,0,0,0)' }); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss2", script2, true);
            string script3 = " $('body').removeClass('modal-open'); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss3", script3, true);
            string script4 = " $('.modal-backdrop').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss4", script4, true);
            string script5 = " $('.modal-backdrop.fade').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss5", script5, true);
            string script6 = " $('.modal-backdrop.fade.in').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss6", script6, true);
        }

        public void mostrarModalPedidos()
        {
            String scriptx = " $(function () {  $(\"#modalOrden\").modal(\"show\"); });";
            ScriptManager.RegisterStartupScript(updDetalle, updDetalle.GetType(), "invokeModal", scriptx, true);
            updPrincipal.Update();
            consultarOrden(int.Parse(Session["CodigoMesaSel"].ToString()));
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            objeto2.cancelarPedido(int.Parse(Session["CodigoPedidoSel"].ToString()), int.Parse(Session["CodigoMesaSel"].ToString()));
            cerrarModalPedidos();
            cargarInformacion();
        }

        protected void btnTotalizarCuenta_Click(object sender, EventArgs e)
        {
            Response.Redirect("WpgFactura.aspx", false);
        }
    }
}

