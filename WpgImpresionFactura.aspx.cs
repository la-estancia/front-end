using AppWebRestaurante.BackEnd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebRestaurante.FrontEnd
{
    public partial class WpgImpresionFactura : System.Web.UI.Page
    {
        ClsPedidos pedido = ClsPedidos.getInstancia();
        ClsPedidos objeto2 = ClsPedidos.getInstancia();
        protected void Page_Load(object sender, EventArgs e)
        {                        
            consultaPresupuestoDetalle();
        }
        private void consultaPresupuestoDetalle()
        {            
            GrdDatos2.DataSource = objeto2.consultarPedidoMesaFactura(int.Parse(Session["CodigoMesaSel"].ToString()));
            GrdDatos2.DataBind();
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Session["ctrl"] = Panel1;
            Control ctrl = (Control)Session["ctrl"];
            PrintHelper.PrintWebControl(ctrl);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("WpgMesas.aspx", false);
        }
    }
}