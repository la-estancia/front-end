using AppWebRestaurante.BackEnd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebRestaurante.FrontEnd
{
    public partial class WpgFactura : System.Web.UI.Page
    {
        ClsPedidos objeto2 = ClsPedidos.getInstancia();
        ClsFactura factura = ClsFactura.getInstancia();
        public Boolean blnError;
        public String error;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                                
                cargarInformacion();
                cargarComboBox();
            }
        }

        public void cargarComboBox()
        {

        }
        public void cargarInformacion()
        {
            updPrincipal.Update();
            GrdDatos2.DataSource = objeto2.consultarPedidoMesa(int.Parse(Session["CodigoMesaSel"].ToString()));
            GrdDatos2.DataBind();
        }
        protected void BtnCrearOrden_Click(object sender, EventArgs e)
        {
            int codigoPedido = int.Parse(Session["CodigoPedidoSel"].ToString());
            int codigoMediaPago = int.Parse(dpdMediaPago.SelectedValue);
            string totalFactura = Session["TotalPedidoSel"].ToString();
            string nit = txtNit.Text;
            factura.registrarFactura(codigoPedido, totalFactura, nit, codigoMediaPago);
            Session["NitSel"] = txtNit.Text;
            Session["FechaSystemaSel"] = System.DateTime.Now;
            Response.Redirect("WpgImpresionFactura.aspx", false);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            String script1 = " $(function () {  $(\"#modalOrdenNueva\").modal(\"show\"); });";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "invokeModal", script1, true);
            updPrincipal.Update();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            cerrarModalPedidos();
            cargarInformacion();
        }
        public void cerrarModalPedidos()
        {
            string script1 = "$('#modalOrden').modal('hide');";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "ssss", script1, true);
            string script2 = " $('.modal-backdrop.fade').css({ 'background-color': 'rgba(0,0,0,0)' }); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss2", script2, true);
            string script3 = " $('body').removeClass('modal-open'); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss3", script3, true);
            string script4 = " $('.modal-backdrop').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss4", script4, true);
            string script5 = " $('.modal-backdrop.fade').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss5", script5, true);
            string script6 = " $('.modal-backdrop.fade.in').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss6", script6, true);
        }
    }
}