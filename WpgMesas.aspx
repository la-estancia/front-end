<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="WpgMesas.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.WpgMesas" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" charset="UTF-8"></script>

<asp:scriptmanager runat="server"></asp:scriptmanager>
<asp:UpdatePanel ID="updPrincipal" UpdateMode="Conditional" runat="server">
<ContentTemplate>

    <asp:MultiView ID="mvlPrincipal" runat="server" ActiveViewIndex="0">
        <asp:View ID="vista1" runat="server">     

              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->                    
                
                <div class="col-md-10 col-md-offset-1">
                
                        <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Mesas restaurante</h3>
                            </div>
                        </center>
                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">            
                            

                 

                                                       
                            <div id="modalOrdenNueva" class="modal fade bd-example-modal" data-keyboard="false" role="dialog" data-backdrop="static">
                                    <div class="modal-dialog" role="document">
                                        <!-- Modal content-->
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">                                       
                                        <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">                                                                                                                                                     
                                                <asp:LinkButton class="close" ID="LinkButton1" runat="server" OnClick="lnkCerrarModal_Click">Cerrar</asp:LinkButton>                                                
                                                <h4 class="modal-title">Apertura de cuenta</h4>
                                            </div>
                                            <div class="modal-body">
                                                    <div class="panel-body">
                                                    <div class="col-md-12">                                                                                                                                                                 
                                                    Actualmente no existe una cuenta activa para esta mesa, ¿Desea crear una? 

                                                    </div>                                                                                                                                                            
                                                    </br></br></br>
                                                    <div class="col-md-12">                                                                                                                                                                 
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Seleccione mesero</label> 
                                                            <asp:DropDownList ID="dpdMesero" class="form-control underlined" data-live-search="true" TabIndex="4" runat="server"></asp:DropDownList>                                            
                                                        </div>                                                                                                       
                                                   
                                                    
                                                    </div>    
                                                    
                                                    </div>

                                                </div>  
                                                <div class="modal-footer">
                                                            
                                                    <asp:Button ID="BtnCrearOrden" runat="server" class="btn btn-primary  " OnClick="BtnCrearOrden_Click" Text="Crear cuenta" />                                                            

                                                </div>                                                
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>





                            
                  <div id="modalOrden" class="modal fade bd-example-modal-lg" data-keyboard="false" role="dialog" data-backdrop="static">
                                    <div class="modal-dialog modal-lg">
                                        <!-- Modal content-->
                                        <asp:UpdatePanel ID="updDetalle" UpdateMode="Conditional" runat="server">                                       
                                        <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">                                                                                                                                                     
                                                <asp:LinkButton class="close" ID="lnkCerrarModal" runat="server" OnClick="lnkCerrarModal_Click">Cerrar</asp:LinkButton>                                                
                                                <h4 class="modal-title">Pedido  Mesa :  <%= Session["CodigoMesaSel"].ToString() %>  </h4>
                                            </div>
                                            <div class="modal-body">
                                                    <div class="panel-body">                                                   
                                                

                                                    <div class="col-md-4">                                                                                                                                                                 
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Producto</label> 
                                                            <asp:DropDownList ID="dpdProducto" class="form-control underlined" data-live-search="true" TabIndex="4" runat="server"></asp:DropDownList>                                            
                                                        </div>                                               

                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Combo</label> 
                                                            <asp:DropDownList ID="dpdCombo" class="form-control underlined" data-live-search="true" TabIndex="1" runat="server"></asp:DropDownList>                                            
                                                        </div>                                                                                                                                    
                                                    </div>
                                                    <div class="col-md-4">  
                                                                                                                                                                     
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Cantidad </label>
                                                            <asp:TextBox ID="txtCantidadproducto" runat="server" class="form-control underlined" name="name" TabIndex="2" type="text" value=""></asp:TextBox>
                                                        </div>    
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Cantidad </label>
                                                            <asp:TextBox ID="txtCantidadCombo" runat="server" class="form-control underlined" name="name" TabIndex="2" type="text" value=""></asp:TextBox>
                                                        </div>                                                        
                                                         <div class="form-group">       
                                                             <label class="control-label" for="name">Total de la cuenta :  </label>
                                                             <asp:TextBox ID="txtTotalCuenta" runat="server" class="form-control underlined" name="name" TabIndex="2" type="text" value="" ReadOnly="true"></asp:TextBox>
                                                          </div>
                                                    </div>
                                                   
                                                    <div class="col-md-4"> 
                                                        <div class="form-group">                          
                                                            <label class="control-label" for="name"></label>                                   
                                                            <asp:Button ID="btnGuardar" runat="server" class="btn btn-primary btn-block btn-lg" OnClick="btnGuardar_Click" Text="Agregar Producto" />
                                                        </div>                                                          
                                                    </div>
                                                     <div class="col-md-4"> 
                                                        <div class="form-group">                          
                                                            <label class="control-label" for="name"></label>                                   
                                                            <asp:Button ID="btnTotalizarCuenta" runat="server" class="btn btn-warning btn-block btn-lg" Text="Facturar Cuenta" onclick="btnTotalizarCuenta_Click"/>
                                                        </div>                                                          
                                                    </div>         
                                                    <div class="col-md-4"> 
                                                        <div class="form-group">                          
                                                            <label class="control-label" for="name"></label>                                   
                                                            <asp:Button ID="Button1" runat="server" class="btn btn-danger btn-block btn-lg" Text="Cancelar Cuenta" onclick="Button1_Click"/>
                                                        </div>                                                          
                                                    </div>         
                                                        <p style="color:red;"><%= error %></p>
                                                </div>

                                            <div class="box-body" >                                                
                                                <div class="midiv" style="overflow: scroll; height: 270px;">    
                                                    <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"   width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoPedido" OnRowCommand="GrdDatos2_RowCommand">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Pedido" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedido" runat="server" CommandName="CodigoPedido" Text='<%# Eval("CodigoPedido") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Detalle" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedidoDetalle" runat="server" CommandName="CodigoPedidoDetalle" Text='<%# Eval("CodigoPedidoDetalle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoProducto" runat="server" CommandName="CodigoProducto" Text='<%# Eval("CodigoProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Combo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoCombo" runat="server" CommandName="CodigoCombo" Text='<%# Eval("CodigoCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombre" runat="server" CommandName="Nombre" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" CommandName="Cantidad" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrecio" runat="server" CommandName="Precio" Text='<%# "Q " + Eval("Precio") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="SubTotal" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSubTotal" runat="server" CommandName="SubTotal" Text='<%# "Q " + String.Format("{0:N2}", Double.Parse(Eval("Cantidad").ToString()) * Double.Parse(Eval("Precio").ToString()))  %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1   %" >
                                                            <ItemTemplate>
                                                                <center>                                                        
                                                                <asp:Button ID="btnEliminar" runat="server" class="btn btn-danger" Text="Eliminar"  CommandName="btnEliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>                                                        
                                                            </center>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                
                                                    </Columns>
                                                    </asp:GridView>                                       
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>

                            <div id="modalError" class="modal fade bd-example-modal-lg" data-keyboard="false" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">Error al crear orden</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="box-header">
                                                <div class="form-group">
                                                    <p>
                                                        <%= error %>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   



                                
                                    <div class="form-group">
                                        <asp:Button Visible = "false" ID="btnAgregar" runat="server" class="btn btn-primary" OnClick="btnAgregar_Click"  Text="Agregar" />
                                    </div>
                                                                
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos" runat="server" class="table table-bordered table-striped"   width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoMesa" OnRowCommand="GrdDatos_RowCommand">
                                            <Columns>
                                                <asp:TemplateField  ItemStyle-VerticalAlign="Top" ItemStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <center>
                                                            <img src="img/table.jpg" class="img-rounded" alt="Cinque Terre" width="100" height="75"> 
                                                        </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Codigo Mesa" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCodigoMesa" runat="server" CommandName="CodigoMesa" Text='<%# Eval("CodigoMesa") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescripcionMesa" runat="server" CommandName="DescripcionMesa" Text='<%# Eval("DescripcionMesa") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField HeaderText="Mesero asignado" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNombreUsuario" runat="server" CommandName="NombreUsuario" Text='<%# Eval("NombreUsuario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField HeaderText="Estado" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEstado" runat="server" CommandName="Estado" Text='<%# Eval("Estado") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1   %">
                                                    <ItemTemplate>
                                                        <center>                                                        
                                                        <asp:Button ID="btnOrdenes" runat="server" class=<%# Eval("Estilo") %> Text="ORDENES MESA" CommandName="btnOrdenes" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>                                                        
                                                    </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                
                                            </Columns>
                                        </asp:GridView>                                       
                                    
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>
                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </asp:View>
        <asp:View ID="vista2" runat="server">      
              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->
                <div class="col-md-8 col-md-offset-2">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Formulario de registro mesas nuevas</h3>
                            </div>
                        </center>

                        <div id="modalError" class="modal fade bd-example-modal-lg" data-keyboard="false" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">Error al actualizar la información</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="box-header">
                                                <div class="form-group">
                                                    <p>
                                                        <%= error %>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                           

                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">                                                                           
                                <div class="row">
                                    <div class="panel-body">
                                        <div class="col-md-10">                                                     
                                            <div class="form-group">
                                                <label class="control-label" for="name">
                                                Descripción </label>
                                                <asp:TextBox ID="txtDescripcion" runat="server" class="form-control underlined" name="name" TabIndex="1" type="text" value=""></asp:TextBox>                                                
                                                <asp:Label ID="Label1" runat="server" Text='<%# error %>' forecolor="Red"></asp:Label>
                                            </div>                                                                                                                                   
                                        </div>                                        
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <br>
                                            <br>
                                        
                                            <asp:Button ID="btnGrabar" runat="server" class="btn btn-primary" Text="Guardar" TabIndex="14" OnClick="btnGrabar_Click" />
                                            <asp:Button ID="btnCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" TabIndex="15" OnClick="btnCancelar_Click" />
                                            <br></br>
                                            </br>
                                            </br>
                                        </div>
                                    </div>
                                </div>                       	                 
                            </div>                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>

        </asp:View>
    </asp:MultiView>
</ContentTemplate> 
</asp:UpdatePanel>   
</asp:Content>
