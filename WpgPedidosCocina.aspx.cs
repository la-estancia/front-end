using AppWebRestaurante.BackEnd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebRestaurante.FrontEnd
{
    public partial class WpgPedidosCocina : System.Web.UI.Page
    {

        ClsPedidos objeto2 = ClsPedidos.getInstancia();        
        public Boolean blnError;
        public String error;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarInformacion();                
            }
        }

        public void cargarInformacion()
        {
            GrdDatos2.DataSource = objeto2.obtenerPedidoCocina();
            GrdDatos2.DataBind();
        }

        protected void BtnCrearOrden_Click(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            cerrarModalPedidos();
        }

        protected void GrdDatos2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GrdDatos2.Rows[index];
            Label lblCodigoPedido = (Label)row.FindControl("lblCodigoPedido");            
            Session["CodigoPedidoSel"] = lblCodigoPedido.Text;
            if (e.CommandName.Equals("btnDetallePedido"))
            {
                GridView1.DataSource = objeto2.obtenerPedidoCocinaDetalle(int.Parse(lblCodigoPedido.Text));
                GridView1.DataBind();
                String script1 = " $(function () {  $(\"#modalOrdenNueva\").modal(\"show\"); });";
                ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "invokeModal", script1, true);
                updPrincipal.Update();
            }

            if (e.CommandName.Equals("btnDespacho"))
            {
                objeto2.actualizarDespacho(int.Parse(lblCodigoPedido.Text));
                mvlPrincipal.ActiveViewIndex = 0;
                cargarInformacion();
                updPrincipal.Update();
            }
        }

        public void cerrarModalPedidos()
        {
            string script1 = "$('#modalOrdenNueva').modal('hide');";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "ssss", script1, true);
            string script2 = " $('.modal-backdrop.fade').css({ 'background-color': 'rgba(0,0,0,0)' }); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss2", script2, true);
            string script3 = " $('body').removeClass('modal-open'); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss3", script3, true);
            string script4 = " $('.modal-backdrop').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss4", script4, true);
            string script5 = " $('.modal-backdrop.fade').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss5", script5, true);
            string script6 = " $('.modal-backdrop.fade.in').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss6", script6, true);
            mvlPrincipal.ActiveViewIndex = 0;
            cargarInformacion();
            updPrincipal.Update();
        }

    }
}