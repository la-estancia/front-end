<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WpgImpresionFactura.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.WpgImpresionFactura" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="btnPrint" runat="server" Text="IMPRIMIR" onclick="btnPrint_Click"/>
        <asp:Button ID="Button1" runat="server" Text="REGRESAR A MESAS" onclick="Button1_Click"/>
        <asp:Panel ID="Panel1" runat="server">                
        <table style="text-align:left"> 
          <tr>
            <th>Nit </th>
            <td><%= Session["NitSel"].ToString() %></td>            
          </tr>
          <tr>
            <th>Fecha Factura </th>
            <td><%= Session["FechaSystemaSel"].ToString() %></td>            
          </tr>          
          <tr>
            <th>Mesero</th>
            <td><%= Session["MeseroSel"].ToString() %></td>            
          </tr>          
        </table>
        </br></br></br>
        <div class="box-body">     
                                        <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoPedido">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Pedido" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedido" runat="server" CommandName="CodigoPedido" Text='<%# Eval("CodigoPedido") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Detalle" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedidoDetalle" runat="server" CommandName="CodigoPedidoDetalle" Text='<%# Eval("CodigoPedidoDetalle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoProducto" runat="server" CommandName="CodigoProducto" Text='<%# Eval("CodigoProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Combo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoCombo" runat="server" CommandName="CodigoCombo" Text='<%# Eval("CodigoCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombre" runat="server" CommandName="Nombre" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" CommandName="Cantidad" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrecio" runat="server" CommandName="Precio" Text='<%# "Q " + Eval("Precio") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                 <p style="color:red;"> <asp:Label ID="lblSubTotall" runat="server" CommandName="SubTotall" Text='TOTAL FACTURA'></asp:Label> </p>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="SubTotal" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSubTotal" runat="server" CommandName="SubTotal" Text='<%# "Q " + String.Format("{0:N2}", Double.Parse(Eval("Cantidad").ToString()) * Double.Parse(Eval("Precio").ToString()))  %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                 <p style="color:red;"> <asp:Label ID="lblSubTotalt" runat="server" CommandName="SubTotalT" Text='<%# "Q " + Session["TotalPedidoSel"].ToString() %>'></asp:Label> </p>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>                                                                                                                
                                                    </Columns>
                                                    </asp:GridView>                                                                           
                                    </div>               
            </asp:Panel>
    </div>
    </form>
</body>
</html>