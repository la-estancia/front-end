<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="ReporteUsuarios.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.ReporteUsuarios" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" charset="UTF-8"></script>

<asp:scriptmanager runat="server"></asp:scriptmanager>
<asp:UpdatePanel ID="updPrincipal" UpdateMode="Conditional" runat="server">
<ContentTemplate>

    <asp:MultiView ID="mvlPrincipal" runat="server" ActiveViewIndex="0">
        <asp:View ID="vista1" runat="server">     

              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->                    
                
                <div class="col-md-10 col-md-offset-1">
                
                        <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Reporte de Usuarios</h3>
                            </div>
                        </center>
                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">                                        
                                   
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoUsuario">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Usuario" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoUsuario" runat="server" CommandName="CodigoUsuario" Text='<%# Eval("CodigoUsuario") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Nombre" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombreUsuario" runat="server" CommandName="NombreUsuario" Text='<%# Eval("NombreUsuario") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Inicio Sesion" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbllogin" runat="server" CommandName="login" Text='<%# Eval("login") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Puesto de trabajo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombreRol" runat="server" CommandName="NombreRol" Text='<%# Eval("NombreRol") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                                                                                
                                                    </Columns>
                                                </asp:GridView>                                                                           
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>
                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </asp:View>

    </asp:MultiView>
</ContentTemplate> 
</asp:UpdatePanel>   
</asp:Content>

