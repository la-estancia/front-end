using AppWebRestaurante.BackEnd;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppWebRestaurante.FrontEnd
{
    public partial class WpgControlCaja : System.Web.UI.Page
    {
        public Boolean blnError;
        public String error;
        ClsMesas objeto = ClsMesas.getInstancia();
        ClsControlCaja controlCaja = ClsControlCaja.getInstancia();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["CodigoMesaSel"] = 0;
                Session["MeseroSel"] = "";
                this.txtFechaSistema.Text = DateTime.Now.ToString();
                cargarInformacion();
                prepararComboBox();
            }
        }

        public void cargarInformacion()
        {
            DataTable consulta = controlCaja.consultarEstadoCaja();
            foreach(DataRow row in consulta.Rows)
            {
                this.txtEstadoCaja.Text = row["Estado"].ToString();
                this.txtFechaUltimoCierre.Text = row["FechaCierre"].ToString();
                this.txtEfectivoApertura.Text = row["EfectivoApertura"].ToString();
                this.txtEfectivoActual.Text = row["EfectivoCierre"].ToString();
                this.txtUsuarioResponsable.Text = row["nombreUsuario"].ToString();

                if (this.txtEstadoCaja.Text.Equals("Abierta"))
                {                    
                    this.btnGrabar.Text = "Cerrar Caja";
                }else
                {
                    this.btnGrabar.Text = "Abrir Caja";
                }
            }
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            if (txtEstadoCaja.Text.Equals("Cerrada"))
            {
                String script1 = " $(function () {  $(\"#modalApertura\").modal(\"show\"); });";
                ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "invokeModal", script1, true);
                updPrincipal.Update();

            }
            else
            {
                if(verificarMesas())
                {
                    controlCaja.cerrarCaja();
                    cargarInformacion();
                }
                
            }
            
        }   
        
        public Boolean verificarMesas()
        {
            DataTable mesas = controlCaja.consultarMesasActivas();
            if (mesas.Rows.Count > 0)
            {
                error = "Error no puede cerrar la caja ya que hay mesas activas";
                String script1 = " $(function () {  $(\"#modalError\").modal(\"show\"); });";
                ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "invokeModal", script1, true);
                updPrincipal.Update();
                return false;
            }
            return true;

        }

        protected void btnAbrirCaja_Click(object sender, EventArgs e)
        {
            int codigoMesero = int.Parse(dpdMesero.SelectedValue.ToString());
            controlCaja.abrirCaja(txtMontoApertura.Text, codigoMesero);
            cerrarModal();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            cerrarModal();   
        }

        public void prepararComboBox()
        {
            dpdMesero.DataSource = objeto.consultarMesero();
            dpdMesero.DataTextField = "NombreUsuario";
            dpdMesero.DataValueField = "CodigoUsuario";
            dpdMesero.DataBind();
        }
           
        public void cerrarModal()
        {
            updPrincipal.Update();
            mvlPrincipal.ActiveViewIndex = 0;
            cargarInformacion();
            string script1 = "$('#modalApertura').modal('hide');";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "ssss", script1, true);
            string script2 = " $('.modal-backdrop.fade').css({ 'background-color': 'rgba(0,0,0,0)' }); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss2", script2, true);
            string script3 = " $('body').removeClass('modal-open'); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss3", script3, true);
            string script4 = " $('.modal-backdrop').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss4", script4, true);
            string script5 = " $('.modal-backdrop.fade').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss5", script5, true);
            string script6 = " $('.modal-backdrop.fade.in').remove(); ";
            ScriptManager.RegisterStartupScript(updPrincipal, updPrincipal.GetType(), "sss6", script6, true);

        }
    }
}