<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="WpgFactura.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.WpgFactura" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" charset="UTF-8"></script>

<asp:scriptmanager runat="server"></asp:scriptmanager>
<asp:UpdatePanel ID="updPrincipal" UpdateMode="Conditional" runat="server">
<ContentTemplate>

    <asp:MultiView ID="mvlPrincipal" runat="server" ActiveViewIndex="0">
        <asp:View ID="vista1" runat="server">     

              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->                    
                
                <div class="col-md-10 col-md-offset-1">
                
                        <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Pago de cuenta</h3>
                            </div>
                        </center>
                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">            
                            

                 

                                                       
                            <div id="modalOrdenNueva" class="modal fade bd-example-modal" data-keyboard="false" role="dialog" data-backdrop="static">
                                    <div class="modal-dialog" role="document">
                                        <!-- Modal content-->
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">                                       
                                        <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">                                                                                                                                                     
                                                <asp:LinkButton class="close" ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Cerrar</asp:LinkButton>                                                
                                                <h4 class="modal-title">Pago de factura</h4>
                                            </div>
                                            <div class="modal-body">
                                                    <div class="panel-body">
                                                    <div class="col-md-12">                                                                                                                                                                 
                                                    Seleccione el medio de pago para pagar la cuenta

                                                    </div>                                                                                                                                                            
                                                    </br></br></br>
                                                    <div class="col-md-12">                                                                                                                                                                 
                                                        <div class="form-group">
                                                            <label class="control-label" for="name">Media de pago</label> 
                                                             <asp:DropDownList ID="dpdMediaPago" class="form-control underlined" data-live-search="true" TabIndex="3" runat="server">
                                                                      <asp:ListItem Text="Efectivo" Value="01" />                                                                     
                                                                      <asp:ListItem Text="Tarjeta de credito" Value="02" />                                                                      
                                                             </asp:DropDownList>   
                                                        </div>                                                                                                       
                                                    </div>                                                        
                                                    </div>

                                                </div>  
                                                <div class="modal-footer">
                                                            
                                                    <asp:Button ID="BtnCrearOrden" runat="server" class="btn btn-primary  " OnClick="BtnCrearOrden_Click" Text="Pagar Factura" />                                                            

                                                </div>                                                
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>




                            <div id="modalError" class="modal fade bd-example-modal-lg" data-keyboard="false" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" data-dismiss="modal" type="button">
                                                ×
                                            </button>
                                            <h4 class="modal-title">Error al crear orden</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="box-header">
                                                <div class="form-group">
                                                    <p>
                                                        <%= error %>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   



                              
                                <div class="row">
                                    
                                        <div class="col-md-6">                                                     
                                            <div class="form-group" onload="myFunction();">
                                                <label class="control-label" for="name">Nit cliente</label>
                                                <asp:TextBox ID="txtNit" runat="server" class="form-control underlined" name="name" TabIndex="1" type="text" value=""></asp:TextBox>                                                                                                
                                            </div>                                                                                                                                   
                                        </div>                                     
                                        <div class="col-md-6">            
                                            <center>
                                                </br>
                                                <asp:Button ID="btnAgregar" runat="server" class="btn btn-primary btn-lg" Text="Facturar" TabIndex="14" OnClick="btnAgregar_Click" />
                                            </center>
                                        </div>
                                                                      
                                </div>                       	                 
                             

                                                                
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoPedido">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Pedido" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedido" runat="server" CommandName="CodigoPedido" Text='<%# Eval("CodigoPedido") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Detalle" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedidoDetalle" runat="server" CommandName="CodigoPedidoDetalle" Text='<%# Eval("CodigoPedidoDetalle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoProducto" runat="server" CommandName="CodigoProducto" Text='<%# Eval("CodigoProducto") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Combo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoCombo" runat="server" CommandName="CodigoCombo" Text='<%# Eval("CodigoCombo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombre" runat="server" CommandName="Nombre" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" CommandName="Cantidad" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPrecio" runat="server" CommandName="Precio" Text='<%# "Q " + Eval("Precio") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                 <p style="color:red;"> <asp:Label ID="lblSubTotall" runat="server" CommandName="SubTotall" Text='TOTAL FACTURA'></asp:Label> </p>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="SubTotal" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSubTotal" runat="server" CommandName="SubTotal" Text='<%# "Q " + String.Format("{0:N2}", Double.Parse(Eval("Cantidad").ToString()) * Double.Parse(Eval("Precio").ToString()))  %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                 <p style="color:red;"> <asp:Label ID="lblSubTotalt" runat="server" CommandName="SubTotalT" Text='<%# "Q " + Session["TotalPedidoSel"].ToString() %>'></asp:Label> </p>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>                                                                                                                
                                                    </Columns>
                                                    </asp:GridView>                                                                           
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>
                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </asp:View>

    </asp:MultiView>
</ContentTemplate> 
</asp:UpdatePanel>   
</asp:Content>