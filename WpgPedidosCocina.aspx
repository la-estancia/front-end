<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="WpgPedidosCocina.aspx.cs" Inherits="AppWebRestaurante.FrontEnd.WpgPedidosCocina" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" charset="UTF-8"></script>

<asp:scriptmanager runat="server"></asp:scriptmanager>
<asp:UpdatePanel ID="updPrincipal" UpdateMode="Conditional" runat="server">
<ContentTemplate>

    <asp:MultiView ID="mvlPrincipal" runat="server" ActiveViewIndex="0">
        <asp:View ID="vista1" runat="server">     

              <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- left column -->                    
                
                <div class="col-md-10 col-md-offset-1">
                
                        <!-- general form elements -->
                    <div class="box box-primary">
                        <center>
                            <div class="box-header with-border">
                                <h3>Ordenes cocina</h3>
                            </div>
                        </center>
                        <!-- /.box-header -->
                        <!-- form start -->                             
                        <div class="box-body">                                        
                              


                            <div id="modalOrdenNueva" class="modal fade bd-example-modal" data-keyboard="false" role="dialog" data-backdrop="static">
                                    <div class="modal-dialog" role="document">
                                        <!-- Modal content-->
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">                                       
                                        <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">                                                                                                                                                     
                                                <asp:LinkButton class="close" ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Cerrar</asp:LinkButton>                                                
                                                <h4 class="modal-title">Detalle de orden </h4>
                                            </div>
                                            <div class="modal-body">                                                    
                                                <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoPedido">
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedidoDetalle" runat="server" CommandName="CodigoPedidoDetalle" Text='<%# Eval("CodigoPedidoDetalle") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Producto " ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombre" runat="server" CommandName="lblNombre" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCantidad" runat="server" CommandName="Cantidad" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        
                                                    </Columns>
                                                    </asp:GridView>                                                                           
                                            </div>                                                                                                  
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>



                                                                
                                <div class="row">
                                    <div class="box-body">                                        
                                        <asp:GridView ID="GrdDatos2" runat="server" class="table table-bordered table-striped"  ShowFooter="true"  width="100%" AutoGenerateColumns="false"  DataKeyNames="CodigoPedido" OnRowCommand="GrdDatos2_RowCommand" >
                                                    <Columns>                                                
                                                        <asp:TemplateField HeaderText="Codigo Pedido" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodigoPedido" runat="server" CommandName="CodigoPedido" Text='<%# Eval("CodigoPedido") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Mesa" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMesa" runat="server" CommandName="Mesa" Text='<%# Eval("Mesa") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Mesero" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="2%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNombreUsuario" runat="server" CommandName="NombreUsuario" Text='<%# Eval("NombreUsuario") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Fecha del pedido" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="5%" Visible ="True">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblfecha" runat="server" CommandName="fecha" Text='<%# Eval("fecha") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1   %" >
                                                            <ItemTemplate>
                                                                <center>                                                        
                                                                <asp:Button ID="btnDetallePedido" runat="server" class="btn btn-success" Text="Detalle Pedido"  CommandName="btnDetallePedido" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>                                                        
                                                            </center>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                

                                                        <asp:TemplateField ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1   %" >
                                                            <ItemTemplate>
                                                                <center>                                                        
                                                                <asp:Button ID="btnDespacho" runat="server" class="btn btn-danger" Text="Despachar Pedido"  CommandName="btnDespacho" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>                                                        
                                                            </center>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                
                                                        
                                                    </Columns>
                                                </asp:GridView>                                                                           
                                    </div>
                                </div>
                                </br>
                                </br>	                 
                            </div>
                        
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </asp:View>

    </asp:MultiView>
</ContentTemplate> 
</asp:UpdatePanel>   
</asp:Content>
